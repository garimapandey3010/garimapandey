<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Car</title>
</head>
<body>
    <div align="center">
        <table border="0" width="90%">
        <form:form action="fetchFeatures" commandName="carForm">
                <tr>
                    <td align="left" width="20%">car: </td>
                    <td align="left" width="40%"><form:input path="carName" size="30"/></td>
                    <td align="left"><form:errors path="carName" cssClass="error"/></td>
                </tr>
                
        </form:form>
        </table>
    </div>
</body>
</html>
package com.car.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.car.form.CarForm;
import com.car.service.CarService;
import com.car.validator.CarValidator;

@Controller
public class CarController {

	@Autowired
	CarService carService;
	
	@Autowired
	CarValidator carValidator;
	
	@InitBinder
	public  void initBinder(WebDataBinder binder) {
        binder.setValidator(carValidator); 
	}
	
	@RequestMapping("/showForm")
	public ModelAndView carForm(Model model){
		   return new ModelAndView("CarForm","command",new CarForm());  
	}
	
	@RequestMapping("/fetchFeatures")
	public String getCarFeatures(@Valid @ModelAttribute("carForm") CarForm carForm,BindingResult result,ModelMap model){
		  String viewName;
		if (result.hasErrors()) {
	           viewName= "CarForm";
	        }
		else{
			String feature=carService.getFeatures();
			model.addAttribute("Feature",feature);
			viewName="ShowFeature";
		}
		return viewName;
	}
}

package com.car.service;

import org.springframework.stereotype.Service;

@Service
public interface CarService {

	public String getFeatures();
}

package com.car.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.car.form.CarForm;


public class CarValidator implements Validator{

	@Override
	public boolean supports(Class<?> clas) {
		return CarForm.class.equals(clas);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors,"carName", "carName.empty");
		
	}

}

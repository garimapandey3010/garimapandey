package com.ford.factory;

import com.ford.model.Car;
import com.ford.model.Ecosport;
import com.ford.model.Figo;

public class CarFactory {

	public static final String ECOSPORT="Ecosport";
	
	public static final String Figo="Figo";
	public Car createCarInstance(String carType){
		
		Car car=null;
		
		if(carType.equalsIgnoreCase(ECOSPORT)){
			car=new Ecosport();
		}
		if(carType.equalsIgnoreCase(Figo)){
			car=new Figo();
		}
		
		return car;
	}
	
	public static void main(String[] args){
		CarFactory carFactory=new CarFactory();
		//Get ecosport light color ---------
		Car car=carFactory.createCarInstance("Ecosport");
		System.out.println(car.features().get("lights"));
	}
}

package com.ford.model;

import java.util.Map;

public interface Car {

	public Map<String,String> features();
}
